import cv2 

def binarizacaoAdaptativo(img, nome):

  suave = cv2.GaussianBlur(img, (5, 5), 0) # aplica blur

  bin1 = cv2.adaptiveThreshold(suave, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY_INV, 21, 5)
  bin2 = cv2.adaptiveThreshold(suave, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY_INV, 21, 5)
  

  cv2.imwrite(nome+"bin.jpg", bin1)

def foo(img, nome):
  
  # deixa cinza
  gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

  # retira ruido
  suave = cv2.blur(gray, (5, 5))

  binarizacaoAdaptativo(suave, nome)

  ret,thresh = cv2.threshold(suave, 0, 255, cv2.THRESH_OTSU)
  
  # im2, contours, hierarchy = cv2.findContours(thresh,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE) 
  # for cnt in contours:
  #   cv2.drawContours(img,[cnt],0,(0,0,255),1) 

  cv2.imwrite(nome, thresh)



img = cv2.imread("dados1.jpg")
img = img[::2,::2] #diminui 4 vezes
foo(img, 'gerado/1.jpg')

img = cv2.imread("dados2.jpg")
img = img[::2,::2] #diminui 4 vezes
foo(img, 'gerado/2.jpg')


# recortar dado unicamente

# verificar se é amarelo ou vermelho

# tratar imagens separadamente