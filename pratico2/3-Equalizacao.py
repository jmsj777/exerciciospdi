from matplotlib import pyplot as plt
import numpy as np
import cv2

def equalizacao(imagem, nome):
    imagem = cv2.cvtColor(imagem, cv2.COLOR_BGR2GRAY)
    h_eq = cv2.equalizeHist(imagem)
    plt.figure()
    plt.title("Histograma Equalizado")
    plt.xlabel("Intensidade")
    plt.ylabel("Qtde de Pixels")
    plt.hist(h_eq.ravel(), 256, [0,256])
    plt.xlim([0, 256])
    plt.savefig(nome+'.png')

    plt.figure()
    plt.title("Histograma Original")
    plt.xlabel("Intensidade")
    plt.ylabel("Qtde de Pixels")
    plt.hist(imagem.ravel(), 256, [0,256])
    plt.xlim([0, 256])
    plt.savefig(nome+'Original.png')


imagem = cv2.imread("manga-1-600.jpg")
equalizacao(imagem, 'gerado/3/1')

imagem = cv2.imread("manga-2-300x208.jpg")
equalizacao(imagem, 'gerado/3/2')

imagem = cv2.imread("manga-3-400x280.jpg")
equalizacao(imagem, 'gerado/3/3')

print("\n Sucesso! \n Salvo dentro de gerado/3")