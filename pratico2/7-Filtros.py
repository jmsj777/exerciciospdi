import numpy as np
import cv2

def sobel(img, nome):

    sobelX = cv2.Sobel(img, cv2.CV_64F, 1, 0)
    sobelY = cv2.Sobel(img, cv2.CV_64F, 0, 1)
    sobelX = np.uint8(np.absolute(sobelX))
    sobelY = np.uint8(np.absolute(sobelY))
    sobel = cv2.bitwise_or(sobelX, sobelY)

    resultado = np.vstack([
        np.hstack([img, sobelX]),
        np.hstack([sobelY, sobel])
        ]) 

    cv2.imwrite(nome, resultado)

def laplaciano(img, nome):

    lap = cv2.Laplacian(img, cv2.CV_64F)
    lap = np.uint8(np.absolute(lap))
    resultado = np.vstack([img, lap]) 

    cv2.imwrite(nome, resultado)



def canny(img, nome):

    suave = cv2.GaussianBlur(img, (7, 7), 0)
    canny1 = cv2.Canny(suave, 20, 120)
    canny2 = cv2.Canny(suave, 70, 200)
    resultado = np.vstack([
        np.hstack([img, suave ]),
        np.hstack([canny1, canny2])
        ]) 

    cv2.imwrite(nome, resultado)


img = cv2.imread("manga-1-600.jpg")
img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY) 
sobel(img, 'gerado/7/1Sobel.png')
laplaciano(img, 'gerado/7/1Laplaciano.png')
canny(img, 'gerado/7/1Canny.png')

img = cv2.imread("manga-2-300x208.jpg")
img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY) 
sobel(img, 'gerado/7/2Sobel.png')
laplaciano(img, 'gerado/7/2Laplaciano.png')
canny(img, 'gerado/7/2Canny.png')

img = cv2.imread("manga-3-400x280.jpg")
img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY) 
sobel(img, 'gerado/7/3Sobel.png')
laplaciano(img, 'gerado/7/3Laplaciano.png')
canny(img, 'gerado/7/3Canny.png')


print("\n Sucesso! \n Salvo dentro de gerado/7")
















