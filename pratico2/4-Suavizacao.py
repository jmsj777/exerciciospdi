import numpy as np
import cv2

def suavizacaoMedia(img, nome):

    suave = np.vstack([
        np.hstack([img, cv2.blur(img, ( 3, 3))]),
        np.hstack([cv2.blur(img, (5,5)), cv2.blur(img, ( 7, 7))]),
        np.hstack([cv2.blur(img, (9,9)), cv2.blur(img, (11, 11))]),
        ])

    cv2.imwrite(nome, suave)

def suavizacaoGaussiana(imagem, nome):

    suave = np.vstack([
        np.hstack([img,
        cv2.GaussianBlur(img, ( 3, 3), 0)]),
        np.hstack([cv2.GaussianBlur(img, ( 5, 5), 0),
        cv2.GaussianBlur(img, ( 7, 7), 0)]),
        np.hstack([cv2.GaussianBlur(img, ( 9, 9), 0),
        cv2.GaussianBlur(img, (11, 11), 0)]),
        ]) 

    cv2.imwrite(nome, suave)

def suavizacaoMediana(imagem, nome):

    suave = np.vstack([
        np.hstack([img,
        cv2.medianBlur(img, 3)]),
        np.hstack([cv2.medianBlur(img, 5),
        cv2.medianBlur(img, 7)]),
        np.hstack([cv2.medianBlur(img, 9),
        cv2.medianBlur(img, 11)]),
        ]) 

    cv2.imwrite(nome, suave)

def suavizacaoBilateral(imagem, nome):

    suave = np.vstack([
        np.hstack([img,
        cv2.bilateralFilter(img, 3, 21, 21)]),
        np.hstack([cv2.bilateralFilter(img, 5, 35, 35),
        cv2.bilateralFilter(img, 7, 49, 49)]),
        np.hstack([cv2.bilateralFilter(img, 9, 63, 63),
        cv2.bilateralFilter(img, 11, 77, 77)])
        ])

    cv2.imwrite(nome, suave)


img = cv2.imread("manga-1-600.jpg")

img = img[::2,::2] # Diminui a imagem

suavizacaoMedia(img, 'gerado/4/1Media.png')
suavizacaoGaussiana(img, 'gerado/4/1Gaussiana.png')
suavizacaoMediana(img, 'gerado/4/1Mediana.png')
suavizacaoBilateral(img, 'gerado/4/1Bilateral.png')

img = cv2.imread("manga-2-300x208.jpg")
img = img[::2,::2] # Diminui a imagem

suavizacaoMedia(img, 'gerado/4/2Media.png')
suavizacaoGaussiana(img, 'gerado/4/2Gaussiana.png')
suavizacaoMediana(img, 'gerado/4/2Mediana.png')
suavizacaoBilateral(img, 'gerado/4/2Bilateral.png')

img = cv2.imread("manga-3-400x280.jpg")
suavizacaoMedia(img, 'gerado/4/3Media.png')
suavizacaoGaussiana(img, 'gerado/4/3Gaussiana.png')
suavizacaoMediana(img, 'gerado/4/3Mediana.png')
suavizacaoBilateral(img, 'gerado/4/3Bilateral.png')

print("\n Sucesso! \n Salvo dentro de gerado/4")










