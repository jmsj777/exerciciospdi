from matplotlib import pyplot as plt 
import cv2

def histogramaCinza(imagem, nome):
    h = cv2.calcHist([imagem], [0], None, [256], [0, 256])
    plt.figure()
    plt.title("Histograma P&B")
    plt.xlabel("Intensidade")
    plt.ylabel("Qtde de Pixels")
    plt.plot(h)
    plt.xlim([0, 256])
    plt.savefig(nome)


imagem = cv2.imread("manga-1-600.jpg")
grey = cv2.cvtColor(imagem, cv2.COLOR_BGR2GRAY)
histogramaCinza(grey, 'gerado/1/1.png')

imagem = cv2.imread("manga-2-300x208.jpg")
grey = cv2.cvtColor(imagem, cv2.COLOR_BGR2GRAY)
histogramaCinza(grey, 'gerado/1/2.png')

imagem = cv2.imread("manga-3-400x280.jpg")
grey = cv2.cvtColor(imagem, cv2.COLOR_BGR2GRAY)
histogramaCinza(grey, 'gerado/1/3.png')

print("\n Sucesso! \n Salvo dentro de gerado/1")