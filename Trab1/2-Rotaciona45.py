import cv2
import numpy as np

def rotaciona(image):
  # grab the image dimensions
  h = image.shape[0]
  w = image.shape[1]

  #rotacao
  alt = h / 2 
  larg = w / 2
  rotacao = cv2.getRotationMatrix2D((alt, larg), 45, 1.0)
  rotacionado = cv2.warpAffine(image, rotacao, (w, h))
  cv2.imshow("Rotacionado 45 graus", rotacionado)
  
  cv2.waitKey(0)
  
  return rotacionado

imagemMoedas1 = cv2.imread("moedasRuido1.jpg")

rota90 = rotaciona(imagemMoedas1)

cv2.imshow("fil1x", rota90)

cv2.imwrite("2-Rotaciona45.jpg", rota90)

cv2.waitKey(0)
cv2.destroyAllWindows()



